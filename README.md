### Minikube

Tre project is based on:
https://theekshanawj.medium.com/kubernetes-deploying-a-nodejs-app-in-minikube-local-development-92df31e0b037

To push image directly to minikube.

eval $(minikube docker-env)
docker build -t starwars-node .
# Additionally you can check to see if starwars-node Docker image is in minikube by
minikube ssh
